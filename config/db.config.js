import Sequelize from "sequelize";
import dbConn from "../config/db.connection.js";

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

/**
 * Include All Models Here
 */
import auth from ('../model/auth.model.js');
db.auth = auth(sequelize, Sequelize);

module.exports = db;