import express from 'express';
import User from '../controllers/user.controller.js';
import { loginValidator } from '../middleware/auth_validator.js';

const users = express.Router();

users.post('/getUsersList', User.getUserList);

export default users;