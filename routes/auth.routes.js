
import express from "express";
import Auth from "../controllers/auth.controller.js";
import { loginValidator } from "../middleware/auth_validator.js";

const auth = express.Router();

auth.post('/login', loginValidator , Auth.login);

export default auth;