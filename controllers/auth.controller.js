import AuthModel from "../models/auth.model.js";
import { getDateTime, getUNQID } from "../utils/common.js";

const Auth = {
    login: async (req, res, next) =>{
        try {
            const req_body = req.body;
            let user = await AuthModel.getByUserName(req_body.email);

            const userMatch = Array.isArray(user) && user.length ? true : false;
            if (!userMatch) return API_RESPONSE.apiFailure(res, 1001);

            const pswdMatch = userMatch ? true : false; //userMatch is temp
            if (!pswdMatch) return API_RESPONSE.apiFailure(res, 1002);

            delete user[0]['password'];

            //=======Generate Unq Auth Token And Log In Auth Table==========//
            const token = await getUNQID(user[0].admin_id);
            user[0].auth_token = token;

            let insert_token_obj = {};

            insert_token_obj['iAdminId'] = user[0]['admin_id'];
            insert_token_obj['vAuthToken'] = user[0]['auth_token'];
            insert_token_obj['dtAddedDate'] = await getDateTime();

            await AuthModel.insertAuthToken(insert_token_obj);
            return API_RESPONSE.apiSuccess(res, 1003, user[0]);  
        } catch (error) {
            next(error)
        }
    }
}
const schemaObj = {

}
export default Auth;