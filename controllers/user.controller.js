import userModel from "../models/user.model.js";

const User = {
    getUserList: async (req, res, next) => {
        try {
            const req_body = req.body;
            let user = await userModel.getUserList();
            return API_RESPONSE.apiSuccess(res, 1003, user[0]); 
        } catch (error) {
            next(error);
        }
    }
}

export default User;