const notFound = (req, res) => {
    API_RESPONSE.apiFailure(res, 'Invalid Endpoint URL !', 404);
};

export default notFound;