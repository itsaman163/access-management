import joi  from "joi";

const loginSchema = joi.object({
    email: joi.string().required(),
    password: joi.string().min(6).required()
});


export const loginValidator = (req, res, next) => {
    const body = req.body;    
    const  result = loginSchema.validate(body)
    if(result.error){
        return API_RESPONSE.apiFailure(res, result.error.message);
    } else {
        next();
    }
}


export const signupValidator = (req, res, next) => {
    const body = req.body;    
    const  result = loginSchema.validate(body)
    if(result.error){
        return API_RESPONSE.apiFailure(res, result.error.message);
    } else {
        next();
    }
}