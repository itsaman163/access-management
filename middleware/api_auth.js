import tokenModel from "../models/auth_token.model.js";

// Refer : https://caffeinecoding.com/leveraging-express-middleware-to-authorize-your-api/
const ApiAuth = async (req, res, next) => {
    try {

        const authtoken = req.headers.authtoken ? req.headers.authtoken : '';

        if (authtoken == '') {
            return API_RESPONSE.apiFailure(res, 'Token missing !', 403);
        }

        const token_data = await tokenModel.getAuthToken(authtoken);

        const isValidToken = token_data ? true : false;
        let not_allowed_keys = ['name','token_id','email','status','username'];
        if (isValidToken) {

            for (const key in token_data) {
                if(!not_allowed_keys.includes(key)) req.body[key] = token_data[key];
            }

            next();
        } else {
            return API_RESPONSE.apiFailure(res, 'Invalid Token !', 401);
        }

    } catch (error) {
        next(error);
    }
}


export default ApiAuth;