import db from "../config/db.connection.js";

const userModel = {
    getUserList: async () => {
        const result = await db
            .select('vName as name', 'vEmail as email', 'vUserName as user_name', 'vEmpCode as code')
            .from('mod_admin')
            .whereRaw('1=1')
        return result;
    }
}
export default userModel;