import db from "../config/db.connection.js";

const AuthModel = {
    getByUserName: async (username) => {
        let user_detail = await db
            .from("mod_admin")
            .select("iAdminId as admin_id", "vEmail as email",
                "vPassword as password", "eStatus as status",
                "vUserName as username", "vName as name",
                "dtAddedDate as added_date"
            )
            .where("vUserName", username);
        return user_detail;
    },
    updateLastAccess: async (admin_id) => {
        await db("mod_admin")
            .update({ dLastAccess: "now()" })
            .where("iAdminId", admin_id);
    },
    insertAuthToken: async (token_arr = {}) => {
        let result = await db.insert(token_arr).into("mod_admin_auth_tokens").then((result) => {
            return result;
        });
        return result;
    },
};
export default AuthModel;

